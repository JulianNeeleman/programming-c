#include <iostream>
#include <stdlib.h>
#include <vector>
#include <cmath>

using namespace std;

const vector<string> digits = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};

void gen(vector<vector<string> > &DP, vector<uint64_t> &cubes, uint64_t &n, uint64_t d) {
    uint64_t cube = n * n * n;
    while(cube <= pow(10, d)) {
        cubes.push_back(cube);
        n++;
        cube = n * n * n;
    }

    if(d > 2) {
        DP.push_back(vector<string>());
        for(unsigned int i = 0; i < DP[d - 2].size(); i++) {
            for(const string &digit : digits) {
                DP[d].push_back(digit + DP[d - 2][i] + digit);
            }
        }
    }
}

bool square(uint64_t n) {
    uint64_t root = sqrt(n);
    return root * root == n;
}

int main()
{
    vector<vector<string> > DP;
    DP.push_back({});
    DP.push_back({"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"});
    DP.push_back({"00", "11", "22", "33", "44", "55", "66", "77", "88", "99"});
    vector<uint64_t> cubes;
    uint64_t found = 0, d = 1, n = 1, answer = 0;
    while(found < 5) {
        gen(DP, cubes, n, d);
        for(const string &cand : DP[d]) {
            if(cand == "0" || cand[0] != '0') {
                uint64_t palindrome = strtoull(cand.c_str(), (char **)NULL, 10), i = 0;
                vector<uint64_t> ways;
                while(cubes[i] < palindrome) {
                    if(square(palindrome - cubes[i])) {
                        ways.push_back(cubes[i]);
                    }
                    i++;
                }
                if(ways.size() == 4) {
                    answer += palindrome;
                    found++;
                    if(found == 5) {
                        cout << answer << endl;
                        return 0;
                    }
                }
            }
        }
        d++;
    }
    return 0;
}
