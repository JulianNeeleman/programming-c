#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

double dist(const int &a, const int &b, const int &n) {
    return sqrt((a + b) * (a + b) - (n - a - b) * (n - a - b));
}

int main()
{
    int n = 100, m = 21;
    vector<vector<double> > d(m, vector<double>(m));
    for(int a = 50 - m + 1; a <= 50; a++) {
        for(int b = a + 1; b <= 50; b++) {
            int i = a - (51 - m), j = b - (51 - m);
            d[i][j] = dist(a, b, n);
            d[j][i] = d[i][j];
        }
    }

    // this hard-coded permutation was found by solving exactly for
    // smaller cases, and looking for the best results of random
    // permutations
    vector<int> perm = {20, 18, 16, 14, 12, 10, 8, 6, 4, 2, 0,
        1, 3, 5, 7, 9, 11, 13, 15, 17, 19};
    double answer = perm[0] + perm[m - 1] + 102 - 2 * m;
    for(int i = 0; i < m - 1; i++) {
        answer += d[perm[i]][perm[i + 1]];
    }
    cout << (int)(answer * 1000) << endl;
    return 0;
}
