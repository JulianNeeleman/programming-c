#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

struct Point {
    double x, y;
};

Point compute_normal(const Point &p) {
    if(p.x > 0.0) {
        return {-1.0, -p.y / (4.0 * p.x)};
    }
    return {1.0, p.y / (4.0 * p.x)};
}

double mult(const vector<vector<double> > &A, const Point &p) {
    return (p.x * A[1][0] + p.y * A[1][1]) / (p.x * A[0][0] + p.y * A[0][1]);
}

int main()
{
    Point p = {0.0, 10.1},
        q = {1.4, -9.6};
    int answer = 0;
    while(abs(q.x) > 0.01 || q.y < 0.0) {
        answer++;
        Point n = compute_normal(q);
        vector<vector<double> > A = {{n.x * n.x - n.y * n.y, 2.0 * n.x * n.y},
            {2.0 * n.x * n.y, n.y * n.y - n.x * n.x}}; // reflection matrix
        double a = mult(A, {p.x - q.x, p.y - q.y}),
            b = q.y - a * q.x,
            D = 400.0 * a * a - 16.0 * b * b + 1600.0,
            first = (-2.0 * a * b + sqrt(D)) / (8 + 2 * a * a),
            second = (-2.0 * a * b - sqrt(D)) / (8 + 2 * a * a);
        p = q;
        if(abs(first - q.x) < abs(second - q.x)) {
            q = {second, a * second + b};
        } else {
            q = {first, a * first + b};
        }
        cout << q.x << " " << q.y << endl;
    }
    cout << answer << endl;
    return 0;
}
