#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <queue>
#include <climits>
#include <iomanip>

using namespace std;

const long double PI = 3.141592653589793238462643383279502884L;

struct Point {
    int x, y, z;

    bool operator<(const Point &rhs) const {
        if(z == rhs.z && y == rhs.y) {
            return x < rhs.x;
        } else if(z == rhs.z) {
            return y < rhs.y;
        }
        return z < rhs.z;
    }
};

int root(const int &n) {
    int root = sqrt(n);
    if(root * root != n) {
        return -1;
    }
    return root;
}

void build_graph(vector<Point> &points, const int &r) {
    int r_sq = r * r;
    for(int x = 0; x <= r; x++) {
        for(int y = x; x * x + y * y <= r_sq; y++) {
            int z = root(r_sq - x * x - y * y);
            if(z >= 0) {
                points.push_back({x, y, z});
                points.push_back({x, y, -z});
            }
        }
    }
}

long double cross_mag(const Point &p, const Point &q) {
    long int x = p.y * q.z - p.z * q.y,
        y = p.z * q.x - p.x * q.z,
        z = p.x * q.y - p.y * q.x,
        magnitude_sq = x * x + y * y + z * z;
    return sqrt(magnitude_sq);
}

long int dot(const Point &p, const Point &q) {
    return p.x * q.x + p.y * q.y + p.z * q.z;
}

long double dist(const Point &p, const Point &q) {
    long double d_sig = atan2(cross_mag(p, q), dot(p, q)),
        d = (d_sig / PI) * (d_sig / PI);
    if(d == 0.0) {
        return 1.0;
    }
    return d;
}

long double dijkstra(const vector<Point> &points, const int &s, const int &t, const int &n) {
    vector<bool> available(n, true);
    priority_queue<pair<long double, int>,
        vector<pair<long double, int> >,
        greater<pair<long double, int> > > q;
    vector<long double> d(n, numeric_limits<long double>::max());
    d[s] = 0.0;
    for(int i = 0; i < n; i++) {
        q.push({d[i], i});
    }
    while(!q.empty()) {
        pair<long double, int> p = q.top();
        q.pop();
        if(available[p.second]) {
            available[p.second] = false;
            for(int j = 0; j < n; j++) {
                if(available[j]) {
                    long double alt = p.first + dist(points[p.second], points[j]);
                    if(alt < d[j]) {
                        d[j] = alt;
                        q.push({d[j], j});
                    }
                }
            }
        }
    }
    return d[t];
}

int main()
{
    long double answer = 0.0;
    for(int n = 1; n <= 15; n++) {
        int r = pow(2, n) - 1;
        vector<Point> points;
        build_graph(points, r);
        sort(points.begin(), points.end());
        long double min_risk = dijkstra(points, 0, points.size() - 1, points.size());
        cout << setprecision(12) << n << " " << points.size() << " " << min_risk << endl;
        answer += min_risk;
    }
    cout << setprecision(12) << answer << endl;
    return 0;
}
