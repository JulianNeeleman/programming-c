#include <iostream>
#include <vector>

using namespace std;

struct Sudoku {
    vector<vector<int> > board;
    vector<vector<bool> > rows, columns, blocks;
    int pos;
};

int get_block(const int &i, const int &j) {
    return 3 * (i / 3) + j / 3;
}

void read_board(Sudoku &s) {
    for(int i = 0; i < 9; i++) {
        for(int j = 0; j < 9; j++) {
            char c;
            cin >> c;
            int v = c - '0';
            s.rows[i][v] = false;
            s.columns[j][v] = false;
            s.blocks[get_block(i, j)][v] = false;
            s.board[i][j] = v;
        }
    }
}

void draw_board(Sudoku &s) {
    for(int i = 0; i < 9; i++) {
        for(int j = 0; j < 9; j++) {
            cout << s.board[i][j];
        }
        cout << endl;
    }
    cout << endl;
}

int get_i(const Sudoku &s) {
    return s.pos / 9;
}

int get_j(const Sudoku &s) {
    return s.pos % 9;
}

void set_value(Sudoku &s, const int &i, const int &j, const int &v) {
    s.rows[i][v] = false, s.columns[j][v] = false;
    s.blocks[get_block(i, j)][v] = false, s.pos++;
    s.board[i][j] = v;
}

void undo_value(Sudoku &s, const int &i, const int &j, const int &v) {
    s.rows[i][v] = true, s.columns[j][v] = true;
    s.blocks[get_block(i, j)][v] = true, s.pos--;
    s.board[i][j] = 0;
}

bool backtrack(Sudoku &s) {
    int i = get_i(s), j = get_j(s);
    if(s.board[i][j] != 0) {
        s.pos++;
        if(s.pos > 80 || backtrack(s)) {
            return true;
        }
        s.pos--;
        return false;
    }
    for(int v = 1; v < 10; v++) {
        if(s.rows[i][v] && s.columns[j][v] && s.blocks[get_block(i, j)][v]) {
            set_value(s, i, j, v);
            if(s.pos == 81 || backtrack(s)) {
                return true;
            }
            undo_value(s, i, j, v);
        }
    }
    return false;
}

int main()
{
    int answer = 0;
    vector<vector<int> > board(9, vector<int>(9, 0));
    vector<vector<bool> > rows(9, vector<bool>(10, true)),
        columns(9, vector<bool>(10, true)),
        blocks(9, vector<bool>(10, true));
    for(int i = 0; i < 50; i++) {
        string ignore;
        cin >> ignore >> ignore;
        Sudoku s = {board, rows, columns, blocks, 0};
        read_board(s);
        backtrack(s);
        //draw_board(s);
        answer += s.board[0][0] * 100 + s.board[0][1] * 10 + s.board[0][2];
    }
    cout << answer << endl;
    return 0;
}
