#include <iostream>
#include <cstdlib>
#include <vector>
#include <cmath>

using namespace std;

uint64_t rec(vector<uint64_t> &fibs, uint64_t n) {
    if(fibs[n] != -1) {
        return fibs[n];
    }
    uint64_t result = rec(fibs, n - 1) + rec(fibs, n - 2);
    fibs[n] = result;
    return result;
}

uint64_t fib(uint64_t n) {
    vector<uint64_t> fibs(n + 1, -1);
    fibs[0] = 0, fibs[1] = 1;
    uint64_t result = rec(fibs, n);
    return result;
}

int main()
{
    vector<int> table(64);
    for(int i = 0; i < 64; i++) {
        bool a = (i >> 5) & 1, b = (i >> 4) & 1, c = (i >> 3) & 1;
        table[i] = (i << 1) % 64 + (a ^ (b && c));
    }
    uint64_t answer = 1;
    vector<bool> alive(64, true);
    for(int i = 0; i < 64; i++) {
        if(alive[i]) {
            int cycle = 0, j = i;
            while(alive[j]) {
                cycle++;
                alive[j] = false;
                j = table[j];
            }
            if(cycle > 1) {
                answer *= fib(cycle + 2) - fib(cycle - 2);
            }
        }
    }
    cout << answer << endl;
    return 0;
}
