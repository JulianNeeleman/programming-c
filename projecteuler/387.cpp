#include <iostream>
#include <cstdint>
#include <cmath>

using namespace std;

const int64_t MAX = 100000000000000;

bool prime(int64_t n) {
    if(n < 2) return false;
    if(n == 2) return true;
    if(n % 2 == 0) return false;
    for(int64_t d = 3; d <= sqrt(n); d += 2) {
        if(n % d == 0) return false;
    }
    return true;
}

int64_t harshad(int64_t n) {
    int64_t d = 0, tmp = n;
    while(tmp > 0) {
        d += tmp % 10;
        tmp /= 10;
    }
    if(n % d != 0) {
        return -1;
    }
    return n / d;
}

void expand(int64_t &answer, int64_t n) {
    int64_t check = harshad(n);
    if(n < MAX / 10 && check != -1) {
        if(prime(check)) {
            for(int i = 0; i < 10; i++) {
                if(n * 10 + i < MAX && prime(n * 10 + i)) {
                    answer += n * 10 + i;
                }
            }
        }
        for(int i = 0; i < 10; i++) {
            expand(answer, n * 10 + i);
        }
    }
}

int main()
{
    int64_t answer = 0;
    for(int d = 1; d < 10; d++) {
        expand(answer, d);
    }
    cout << answer << endl;
    return 0;
}
