#include <iostream>
#include <vector>

using namespace std;

struct Interval {
    int from, to;
};

int main()
{
    int T;
    cin >> T;
    for(int t = 1; t <= T; t++) {
        int N;
        cin >> N;
        vector<Interval> top, bottom;
        int from = 0;
        bool open = false;
        for(int i = 0; i < N; i++) {
            char c;
            cin >> c;
            if(c == '.' && !open) {
                from = i, open = true;
            } else if(c == 'X' && open) {
                top.push_back({from, i - 1});
                open = false;
            }
            if(open && i == N - 1) {
                top.push_back({from, i});
            }
        }
        from = 0, open = false;
        for(int i = 0; i < N; i++) {
            char c;
            cin >> c;
            if(c == '.' && !open) {
                from = i, open = true;
            } else if(c == 'X' && open) {
                bottom.push_back({from, i - 1});
                open = false;
            }
            if(open && i == N - 1) {
                bottom.push_back({from, i});
            }
        }
        int answer = top.size() + bottom.size();
        auto j = bottom.begin();
        for(auto i = top.begin(); i != top.end(); i++) {
            while(j != bottom.end() && j->to <= i->to) {
                if(j->from == j->to && j->from >= i->from) {
                    answer--;
                    break;
                }
                j++;
            }
        }
        j = top.begin();
        for(auto i = bottom.begin(); i != bottom.end(); i++) {
            while(i->from < i->to && j != top.end() && j->to <= i->to) {
                if(j->from == j->to && j->from >= i->from) {
                    answer--;
                    break;
                }
                j++;
            }
        }
        cout << "Case #" << t << ": " << answer << endl;
    }
    return 0;
}
