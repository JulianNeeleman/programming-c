#include <iostream>
#include <set>

using namespace std;

struct Point {
    int x, y;

    bool operator<(const Point &rhs) const {
        return x == rhs.x ? y < rhs.y : x < rhs.x;
    }
};

struct Distance {
    int l, n;

    bool operator==(const Distance &rhs) const {
        return l == rhs.l;
    }

    bool operator<(const Distance &rhs) const {
        return l < rhs.l;
    }
};

int dist(const Point &p, const Point &q) {
    return (p.x - q.x) * (p.x - q.x) + (p.y - q.y) * (p.y - q.y);
}

int main()
{
    int T;
    cin >> T;
    for(int i = 1; i <= T; i++) {
        int N;
        cin >> N;
        set<Point> stars;
        for(int j = 0; j < N; j++) {
            Point p;
            cin >> p.x >> p.y;
            stars.insert(p);
        }
        int answer = 0;
        for(auto j = stars.begin(); j != stars.end(); j++) {
            set<Distance> distances;
            for(auto k = stars.begin(); k != stars.end(); k++) {
                Distance d = {dist((*j), (*k)), 1};
                auto it = distances.find(d);
                if(it != distances.end()) {
                    d.n = (*it).n + 1;
                    distances.erase(it);
                }
                distances.insert(d);
            }
            for(auto it = distances.begin(); it != distances.end(); it++) {
                int s = (*it).n;
                answer += (s * (s - 1)) / 2;
            }
        }
        cout << "Case #" << i << ": " << answer << endl;
    }
    return 0;
}
