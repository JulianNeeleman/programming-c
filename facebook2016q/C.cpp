#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int T;
    cin >> T;
    for(int t = 1; t <= T; t++) {
        int N, P;
        cin >> N >> P;
        vector<int> boxes(N + 1, 0);
        for(int i = 0; i < N; i++) {
            cin >> boxes[i];
        }
        int from = 0, to = 0, v = boxes[0];
        int64_t answer = 0;
        while(to < N) {
            if(v <= P) {
                to++, answer += (int64_t)(to - from), v += boxes[to];
            } else {
                v -= boxes[from], from++;
            }
        }
        cout << "Case #" << t << ": " << answer << endl;
    }
    return 0;
}
