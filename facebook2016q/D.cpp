#include <iostream>
#include <vector>
#include <set>

using namespace std;

struct Node {
    bool w;
    int s;
    vector<int> children;
};

const int M = 1000000000;

string common(const string &a, const string &b) {
    int n = min(a.size(), b.size());
    for(int i = 0; i < n; i++) {
        if(a[i] != b[i]) return a.substr(0, i);
    }
    return a.substr(0, n);
}

vector<int> rec(const vector<Node> &tree, const int root, const int &K) {
    vector<int> DP(K + 1, M);
    if(tree[root].w) DP[1] = 0;
    for(auto child : tree[root].children) {
        vector<int> sub = rec(tree, child, K);
        int dist = 2 * (tree[child].s - tree[root].s);
        for(int i = K; i > 0; i--) {
            for(int j = 1; j <= K - i; j++) {
                DP[i + j] = min(DP[i + j], sub[j] + DP[i] + dist);
            }
            DP[i] = min(DP[i], sub[i] + dist);
        }
    }
    return DP;
}

int main()
{
    int T;
    cin >> T;
    for(int t = 1; t <= T; t++) {
        int N, K;
        cin >> N >> K;
        set<string> original;
        for(int i = 0; i < N; i++) {
            string w;
            cin >> w;
            original.insert(w);
        }
        int n = 0;
        set<string> words(original.begin(), original.end());
        words.insert("");
        while(n != words.size()) {
            n = words.size();
            for(auto i = words.begin(), j = next(words.begin()); j != words.end(); i++, j++) {
                words.insert(common(*i, *j));
            }
        }
        vector<string> nodes(words.begin(), words.end());
        vector<Node> tree(n);
        for(int i = 0; i < n; i++) {
            if(original.find(nodes[i]) != original.end()) {
                tree[i].w = true;
            }
            tree[i].s = nodes[i].size();
            for(int j = i - 1; j >= 0; j--) {
                if(nodes[j] == common(nodes[i], nodes[j])) {
                    tree[j].children.push_back(i);
                    break;
                }
            }
        }
        vector<int> result = rec(tree, 0, K);
        cout << "Case #" << t << ": " << result[K] + K << endl;
    }
}
