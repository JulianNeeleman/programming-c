#include <iostream>
#include <vector>
#include <cmath>
#include <iomanip>
#include <algorithm>

using namespace std;

const double r = 6370.0,
    epsilon = 0.00001;

struct Point {
    double x, y, z;
};

Point cartesian(double lat, double lon) {
    lat *= M_PI / 180.0, lon *= M_PI / 180.0;
    return {cos(lat) * cos(lon),
        cos(lat) * sin(lon),
        sin(lat)};
}

double magnitude(const Point &p) {
    return sqrt(p.x * p.x + p.y * p.y + p.z * p.z);
}

Point normalize(const Point &p) {
    double length = magnitude(p);
    return {p.x / length,
        p.y / length,
        p.z / length};
}

Point cross(const Point &p, const Point &q) {
    return {p.y * q.z - p.z * q.y,
        p.z * q.x - p.x * q.z,
        p.x * q.y - p.y * q.x};
}

double dot(const Point &p, const Point &q) {
    return p.x * q.x + p.y * q.y + p.z * q.z;
}

double dist(const Point &p, const Point &q) {
    return atan2(magnitude(cross(p, q)), dot(p, q));
}

Point negation(const Point &p) {
    return {-p.x, -p.y, -p.z};
}

pair<Point, Point> compute_intersections(const Point &p, const Point &q, const Point &s, const Point &t) {
    Point v1 = cross(p, q), v2 = cross(s, t),
        d = cross(v1, v2),
        first = normalize(d), second = negation(first);
    return make_pair(first, second);
}

bool on_arc(const Point &p, const Point &q, const Point &s) {
    return abs(dist(p, q) - dist(p, s) - dist(q, s)) < epsilon;
}

int main()
{
    int c;
    cin >> c;
    vector<pair<Point, Point> > edges;
    for(int i = 0; i < c; i++) {
        int n;
        cin >> n;
        vector<Point> points(n);
        for(int j = 0; j < n; j++) {
            double lat, lon;
            cin >> lat >> lon;
            points[j] = cartesian(lat, lon);
        }
        for(int j = 0; j < n; j++) {
            edges.push_back(make_pair(points[j], points[(j + 1) % n]));
        }
    }
    int m;
    cin >> m;
    vector<Point> waypoints(m);
    double l = 0.0, w = 0.0;
    bool water = false;
    for(int i = 0; i < m; i++) {
        double lat, lon;
        cin >> lat >> lon;
        waypoints[i] = cartesian(lat, lon);
        if(i > 0) {
            double d = dist(waypoints[i - 1], waypoints[i]);
            l += d;
            vector<double> crossings;
            for(const pair<Point, Point> &e : edges) {
                pair<Point, Point> intersections = compute_intersections(waypoints[i - 1], waypoints[i], e.first, e.second);
                if(on_arc(waypoints[i - 1], waypoints[i], intersections.first) &&
                    on_arc(e.first, e.second, intersections.first)) {
                    crossings.push_back(dist(waypoints[i - 1], intersections.first));
                } else if(on_arc(waypoints[i - 1], waypoints[i], intersections.second) &&
                    on_arc(e.first, e.second, intersections.second)) {
                    crossings.push_back(dist(waypoints[i - 1], intersections.second));
                }
            }
            sort(crossings.begin(), crossings.end());
            double last = 0.0;
            for(const double &crossing : crossings) {
                if(water) w += crossing - last;
                water = !water, last = crossing;
            }
            if(water) w += d - last;
        }
    }
    cout << setprecision(20) << l * r << " " << 100.0 * w / l << endl;
    return 0;
}
