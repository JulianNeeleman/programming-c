#include <iostream>
#include <vector>

using namespace std;

struct Point {
    int x, y;

    bool operator==(const Point &rhs) const {
        return x == rhs.x && y == rhs.y;
    }

    bool operator!=(const Point &rhs) const {
        return x != rhs.x || y != rhs.y;
    }
};

struct Pipe {
    Point from, to;
};

int orientation(const Point &a, const Point &b, const Point &c) {
    int result = (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);
    return (result > 0) - (result < 0);
}

bool intersect(const Pipe &a, const Pipe &b) {
    return a.to == b.to || (a.from != b.from &&
        orientation(a.from, a.to, b.from) != orientation(a.from, a.to, b.to) &&
        orientation(b.from, b.to, a.from) != orientation(b.from, b.to, a.to));
}

void build_graph(vector<vector<int> > &graph, const vector<Pipe> &pipes, const int &p) {
    for(int i = 0; i < p; i++) {
        for(int j = i + 1; j < p; j++) {
            if(intersect(pipes[i], pipes[j])) {
                graph[i].push_back(j);
                graph[j].push_back(i);
            }
        }
    }
}

bool dfs(const vector<vector<int> > &graph, vector<int>& colors, const int &i, const int &color) {
    if(colors[i] != 0 && colors[i] != color) {
        return false;
    }
    if(colors[i] == 0) {
        colors[i] = color;
        for(unsigned int j = 0; j < graph[i].size(); j++) {
            if(!dfs(graph, colors, graph[i][j], -color)) {
                return false;
            }
        }
    }
    return true;
}

bool bipartite(const vector<vector<int> > &graph, const int &p) {
    vector<int> colors(p, 0);
    for(int i = 0; i < p; i++) {
        if(colors[i] == 0 && !dfs(graph, colors, i, 1)) {
            return false;
        }
    }
    return true;
}

int main() {
    int w, p;
    cin >> w >> p;
    vector<Point> wells(w);
    for(int i = 0; i < w; i++) {
        Point well;
        cin >> wells[i].x >> wells[i].y;
    }
    vector<Pipe> pipes(p);
    for(int i = 0; i < p; i++) {
        int s, x, y;
        cin >> s >> pipes[i].to.x >> pipes[i].to.y;
        pipes[i].from.x = wells[s - 1].x;
        pipes[i].from.y = wells[s - 1].y;
    }
    vector<vector<int> > graph(p);
    build_graph(graph, pipes, p);
    if(bipartite(graph, p)) {
        cout << "possible" << endl;
    } else {
        cout << "impossible" << endl;
    }
    return 0;
}
