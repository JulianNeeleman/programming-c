#include <iostream>
#include <vector>
#include <cstdint>

using namespace std;

void permute(vector<int> &order, const vector<int> &first, const vector<int> &second, int n) {
    vector<int> inverse(n);
    for(int i = 0; i < n; i++) {
        inverse[first[i] - 1] = i;
    }
    for(int i = 0; i < n; i++) {
        order[i] = inverse[second[i] - 1] + 1;
    }
}

// submerge, get it? :D
int64_t sub_merge(const vector<int> &order, vector<int> &buffer, const int &from, const int &middle, const int &to) {
    int i = from, j = middle;
    int64_t inversions = 0;
    for(int k = from; k < to; k++) {
        if(i < middle && (j >= to || order[i] <= order[j])) {
            buffer[k] = order[i++];
        } else {
            buffer[k] = order[j++];
            inversions += middle - i;
        }
    }
    return inversions;
}

void copy_results(vector<int> &order, const vector<int> &buffer, const int &from, const int &to) {
    for(int k = from; k < to; k++) {
        order[k] = buffer[k];
    }
}

int64_t split_merge(vector<int> &order, vector<int> &buffer, const int &from, const int &to) {
    if(to - from < 2) {
        buffer[from] = order[from];
        return 0;
    }
    int middle = (from + to + 1) / 2;
    int64_t inversions = split_merge(order, buffer, from, middle) +
        split_merge(order, buffer, middle, to) +
        sub_merge(order, buffer, from, middle, to);
    copy_results(order, buffer, from, to);
    return inversions;
}

int64_t merge_sort(const vector<int> &first, const vector<int> &second, const int &n) {
    vector<int> order(n), buffer(n);
    permute(order, first, second, n);
    return split_merge(order, buffer, 0, n);
}

int main()
{
    int64_t n;
    cin >> n;
    vector<int> jeroen(n), vincent(n), julian(n);
    for(int i = 0; i < n; i++) {
        cin >> jeroen[i];
    }
    for(int i = 0; i < n; i++) {
        cin >> vincent[i];
    }
    for(int i = 0; i < n; i++) {
        cin >> julian[i];
    }
    cout << (n * (n - 1) - merge_sort(jeroen, vincent, n) -
        merge_sort(jeroen, julian, n) -
        merge_sort(vincent, julian, n)) / 2 << endl;
    return 0;
}
