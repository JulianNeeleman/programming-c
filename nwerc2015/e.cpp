#include <iostream>
#include <climits>
#include <vector>
#include <queue>
#include <set>
#include <map>

using namespace std;

struct Node {
    int match;
    int64_t a, b;
    set<int> adj;
};

bool bfs(vector<Node>& U, vector<Node>& V, vector<int>& dist) {
    queue<int> Q;
    for(unsigned int i = 1; i < U.size(); i++) {
        if(U[i].match == 0) {
            dist[i] = 0;
            Q.push(i);
        } else {
            dist[i] = INT_MAX;
        }
    }
    dist[0] = INT_MAX;
    while(!Q.empty()) {
        int i = Q.front();
        Q.pop();
        if(dist[i] < dist[0]) {
            for(const int &j : U[i].adj) {
                if(dist[V[j].match] == INT_MAX) {
                    dist[V[j].match] = dist[i] + 1;
                    Q.push(V[j].match);
                }
            }
        }
    }
    return dist[0] != INT_MAX;
}

bool dfs(vector<Node>& U, vector<Node>& V, vector<int>& dist, int i) {
    if(i != 0) {
        for(const int &j : U[i].adj) {
            if(dist[V[j].match] == dist[i] + 1) {
                if(dfs(U, V, dist, V[j].match)) {
                    V[j].match = i;
                    U[i].match = j;
                    return true;
                }
            }
        }
        dist[i] = INT_MAX;
        return false;
    }
    return true;
}

int hopcroft_karp(vector<Node>& U, vector<Node>& V, vector<int>& dist) {
    int matching = 0;
    while(bfs(U, V, dist)) {
        for(unsigned int i = 1; i < U.size(); i++) {
            if(U[i].match == 0) {
                if(dfs(U, V, dist, i)) {
                    matching++;
                }
            }
        }
    }
    return matching;
}

int main()
{
    int n;
    cin >> n;
    vector<Node> U(n + 1, {0, 0, 0, set<int>()}), V(1);
    vector<int> dist(n + 1, INT_MAX);
    map<int64_t, int> indexing;
    int index = 1;
    for(int i = 1; i <= n; i++) {
        int64_t a, b;
        cin >> a >> b;
        if(indexing[a + b] == 0) {
            V.push_back({0, 0, 0, set<int>()});
            indexing[a + b] = index++;
        }
        if(indexing[a - b] == 0) {
            V.push_back({0, 0, 0, set<int>()});
            indexing[a - b] = index++;
        }
        if(indexing[a * b] == 0) {
            V.push_back({0, 0, 0, set<int>()});
            indexing[a * b] = index++;
        }
        U[i].a = a, U[i].b = b;
        U[i].adj.insert(indexing[a + b]);
        U[i].adj.insert(indexing[a - b]);
        U[i].adj.insert(indexing[a * b]);
        V[indexing[a + b]].adj.insert(i);
        V[indexing[a - b]].adj.insert(i);
        V[indexing[a * b]].adj.insert(i);
    }
    int matching = hopcroft_karp(U, V, dist);
    if(matching != n) {
        cout << "impossible" << endl;
    } else {
        for(unsigned int i = 1; i < U.size(); i++) {
            if(U[i].match == indexing[U[i].a + U[i].b]) {
                cout << U[i].a << " + " << U[i].b << " = " << U[i].a + U[i].b << endl;
            } else if(U[i].match == indexing[U[i].a - U[i].b]) {
                cout << U[i].a << " - " << U[i].b << " = " << U[i].a - U[i].b << endl;
            } else if(U[i].match == indexing[U[i].a * U[i].b]) {
                cout << U[i].a << " * " << U[i].b << " = " << U[i].a * U[i].b << endl;
            }
        }
    }
    return 0;
}
