#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int T;
    cin >> T;
    while(T--) {
        int D, X, Y;
        cin >> D >> X >> Y;
        int Z = X * X + Y * Y;
        if(Z == 0) {
            cout << 0 << endl;
        } else if(Z < D) {
            cout << 2 << endl;
        } else {
            int square = (Z + D - 1) / D,
                root = sqrt(square);
            cout << root + (root * root != square) << endl;
        }
    }
    return 0;
}
