#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

struct Point {
    int x, y;
};

int area(const vector<Point> &polygon, const int &C) {
    int A = 0;
    for(int i = 1; i < C; i++) {
        A += polygon[i].x *
            (polygon[i + 1].y -
            polygon[i - 1].y);
    }
    return abs(A / 2);
}

int main()
{
    int T;
    cin >> T;
    while(T--) {
        int C;
        cin >> C;
        vector<Point> polygon(C + 2);
        int x = 0, y = 0;
        for(int i = 0; i < C; i++) {
            polygon[i].x = x, polygon[i].y = y;
            char d;
            int l;
            cin >> d >> l;
            x += (1 + (d == 'x')) * ((d != 'z') * 2 - 1) * l;
            y += (d != 'x') * l;
        }
        polygon[C] = polygon[0];
        polygon[C + 1] = polygon[1];
        cout << area(polygon, C + 1) << endl;
    }
    return 0;
}
