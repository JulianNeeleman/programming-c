#include <iostream>
#include <vector>

using namespace std;

int largest(const vector<int> &chars, const int &last) {
    int highest = 0, result = -1;
    for(int i = 0; i < 26; i++) {
        if(i != last && chars[i] > highest) {
            highest = chars[i];
            result = i;
        }
    }
    return result;
}

int main()
{
    int T;
    cin >> T;
    string word;
    while(getline(cin, word)) {
        vector<int> chars(26, 0);
        int n = word.size();
        for(int i = 0; i < n; i++) {
            chars[word[i] - 97]++;
        }
        string answer = "";
        char last = '#';
        for(int i = 0; i < n; i++) {
            int j = largest(chars, last - 97);
            if(j < 0) {
                answer = "IMPOSSIBLE";
                break;
            }
            chars[j]--;
            last = (char)('a' + j);
            answer += last;
        }
        cout << answer << endl;
    }
    return 0;
}
