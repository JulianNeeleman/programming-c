#include <iostream>
#include <vector>

using namespace std;

struct Station {
    int G, D;
};

int main()
{
    ios::sync_with_stdio(false);
    int T;
    cin >> T;
    while(T--) {
        int N;
        cin >> N;
        vector<Station> iceland(N);
        for(int i = 0; i < N; i++) {
            cin >> iceland[i].G >> iceland[i].D;
        }
        int from = 0, to = 0, g = 0;
        do {
            if(g < 0) {
                from = (from - 1 + N) % N, g += iceland[from].G - iceland[from].D;
            } else {
                g += iceland[to].G - iceland[to].D, to = (to + 1) % N;
            }
        } while(from != to);
        g < 0 ? cout << "IMPOSSIBLE" << endl : cout << from + 1 << endl;
    }
    return 0;
}
