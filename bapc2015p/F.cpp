#include <iostream>
#include <vector>

using namespace std;

struct Point {
    int x, y;
    bool powered;

    bool operator==(const Point &rhs) const {
        return x == rhs.x && y == rhs.y;
    }

    bool operator!=(const Point &rhs) const {
        return x != rhs.x || y != rhs.y;
    }
};

Point root(vector<vector<Point> > &grid, const Point &u) {
    if(grid[u.x][u.y] != u) {
        grid[u.x][u.y] = root(grid, grid[u.x][u.y]);
    }
    return grid[u.x][u.y];
}

void unite(vector<vector<Point> > &grid, const Point &u, const Point &v) {
    Point u_root = root(grid, u),
        v_root = root(grid, v);
    grid[v_root.x][v_root.y].powered = v_root.powered || u_root.powered;
    grid[u_root.x][u_root.y] = v_root;
}

int main()
{
    int T;
    cin >> T;
    while(T--) {
        int n, m, p, c;
        cin >> n >> m >> p >> c;
        vector<vector<Point> > grid(n, vector<Point>(m));
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < m; j++) {
                grid[i][j] = {i, j, false};
            }
        }
        for(int i = 0; i < p; i++) {
            int x, y;
            cin >> x >> y;
            grid[x][y].powered = true;
        }
        for(int i = 0; i < c; i++) {
            int x, y;
            char d;
            cin >> x >> y >> d;
            unite(grid, {x, y, false}, {x + (d == 'R'), y + (d == 'U'), false});
        }
        int k = 0;
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < m; j++) {
                Point root = {i, j, false};
                k += grid[i][j] == root && !grid[i][j].powered;
            }
        }
        cout << k << endl;
    }
    return 0;
}
