#include <iostream>
#include <vector>
#include <tuple>
#include <set>

using namespace std;

struct State {
    int x1, y1, x2, y2;
    vector<char> path;

    bool operator<(const State &rhs) const {
        return tie(x1, y1, x2, y2) < tie(rhs.x1, rhs.y1, rhs.x2, rhs.y2);
    }

    bool synced() {
        return x1 == x2 && y1 == y2;
    }
};

vector<char> success(const set<State> &states) {
    for(set<State>::iterator it = states.begin(); it != states.end(); it++) {
        State s = *it;
        if(s.synced()) {
            return s.path;
        }
    }
    return vector<char>();
}

State play(const vector<vector<char> > &maze, const int &M, const int &N, State s, const char &d) {
    s.path.push_back(d);
    int new_x1, new_y1, new_x2, new_y2;
    if(d == 'N') {
        new_x1 = (s.x1 - 1 + M) % M, new_y1 = s.y1;
        new_x2 = (s.x2 - 1 + M) % M, new_y2 = s.y2;
    } else if(d == 'E') {
        new_x1 = s.x1, new_y1 = (s.y1 + 1) % N;
        new_x2 = s.x2, new_y2 = (s.y2 + 1) % N;
    } else if(d == 'S') {
        new_x1 = (s.x1 + 1) % M, new_y1 = s.y1;
        new_x2 = (s.x2 + 1) % M, new_y2 = s.y2;
    } else {
        new_x1 = s.x1, new_y1 = (s.y1 - 1 + N) % N;
        new_x2 = s.x2, new_y2 = (s.y2 - 1 + N) % N;
    }
    if(maze[new_x1][new_y1] == 'G' || maze[new_x2][new_y2] == 'G') {
        return s;
    }
    if(maze[new_x1][new_y1] == '.') {
        s.x1 = new_x1, s.y1 = new_y1;
    }
    if(maze[new_x2][new_y2] == '.') {
        s.x2 = new_x2, s.y2 = new_y2;
    }
    return s;
}

int main()
{
    int T;
    cin >> T;
    while(T--) {
        int M, N;
        cin >> M >> N;
        vector<vector<char> > maze(M, vector<char>(N));
        set<State> states, memory;
        State s = {-1, -1, -1, -1, vector<char>()};
        for(int i = 0; i < M; i++) {
            for(int j = 0; j < N; j++) {
                cin >> maze[i][j];
                if(maze[i][j] == 'P' && s.x1 == -1) {
                    s.x1 = i, s.y1 = j;
                    maze[i][j] = '.';
                } else if(maze[i][j] == 'P') {
                    s.x2 = i, s.y2 = j;
                    states.insert(s);
                    maze[i][j] = '.';
                }
            }
        }
        vector<char> result = success(states);
        while(!states.empty() && result.empty()) {
            set<State> new_states;
            for(set<State>::iterator it = states.begin(); it != states.end(); it++) {
                for(const char &d : {'N', 'E', 'S', 'W'}) {
                    State new_state = play(maze, M, N, *it, d);
                    if(memory.find(new_state) == memory.end()) {
                        new_states.insert(new_state);
                        memory.insert(new_state);
                    }
                }
            }
            states = new_states;
            result = success(states);
        }
        if(result.empty()) {
            cout << "IMPOSSIBLE" << endl;
        } else {
            cout << result.size() << " ";
            for(const char &c : result) {
                cout << c;
            }
            cout << endl;
        }
    }
    return 0;
}
