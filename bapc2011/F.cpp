#include <iostream>
#include <set>
#include <tuple>

using namespace std;

struct Solution {
    int64_t N, E, S, W;

    bool operator<(const Solution &rhs) const {
        return tie(N, E, S, W) < tie(rhs.N, rhs.E, rhs.S, rhs.W);
    }
};

int64_t abs(const int64_t &a) {
    return a < 0 ? -a : a;
}

void attempt(set<Solution> &solutions, int64_t &dist, const int64_t &a, const int64_t &b, const int64_t &c, const int64_t &d,
             const int64_t &xs, const int64_t &ys, const int64_t &x1, const int64_t &x2, const int64_t &y1, const int64_t& y2) {
    Solution sol = {abs(d), abs(b), abs(c), abs(a)};
    int64_t x = a * x1 + b * x2, y = c * y1 + d * y2,
        new_dist = (x - xs) * (x - xs) + (y - ys) * (y - ys);
    if(solutions.empty() || new_dist < dist) {
        solutions.clear();
        solutions.insert(sol);
        dist = new_dist;
    } else if(!solutions.empty() && new_dist == dist) {
        solutions.insert(sol);
    }
}

int main()
{
    int T;
    cin >> T;
    while(T--) {
        int64_t L, W, B, xs, ys, xf, yf;
        cin >> L >> W >> B >> xs >> ys >> xf >> yf;
        int64_t x1 = 2 * xf, x2 = 2 * (L - xf), y1 = 2 * yf, y2 = 2 * (W - yf),
            p = 0, q = 0, r = B / 2 + (B % 2 == 1), s = B / 2;
        xs -= xf, ys -= yf;
        set<Solution> solutions;
        int64_t dist = 0;
        for(int64_t n = 0; n <= B; n++) {
            attempt(solutions, dist, -p, -q, -r, -s, xs, ys, x1, x2, y1, y2);
            attempt(solutions, dist, -p, -q, s, r, xs, ys, x1, x2, y1, y2);
            attempt(solutions, dist, q, p, -r, -s, xs, ys, x1, x2, y1, y2);
            attempt(solutions, dist, q, p, s, r, xs, ys, x1, x2, y1, y2);
            n % 2 == 0 ? p++ : q++;
            (B - n) % 2 == 1 ? r-- : s--;
        }
        for(const Solution &sol : solutions) {
            cout << sol.N << " " << sol.E << " " << sol.S << " " << sol.W << endl;
        }
        cout << 0 << endl;
    }
    return 0;
}
