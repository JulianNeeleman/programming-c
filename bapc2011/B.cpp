#include <iostream>
#include <vector>

using namespace std;

struct Point {
    int i, j, d;
};

void play(vector<vector<char> > &grid, vector<Point> &new_expands,
    const int &i, const int &j, const int &d) {
    if(grid[i][j] == '.') {
        new_expands.push_back({i, j, 0});
    } else if(grid[i][j] == '@') {
        new_expands.push_back({i, j, d});
    }
    grid[i][j] = '#';
}

int main()
{
    int T;
    cin >> T;
    while(T--) {
        int h, w, d;
        cin >> h >> w >> d;
        vector<vector<char> > grid(h, vector<char>(w));
        vector<Point> expands;
        for(int i = 0; i < h; i++) {
            for(int j = 0; j < w; j++) {
                cin >> grid[i][j];
                if(grid[i][j] == 'S') {
                    expands.push_back({i, j, 0});
                    grid[i][j] = '#';
                }
            }
        }
        bool found = false;
        int answer = 0;
        while(!found) {
            answer++;
            vector<Point> new_expands;
            for(const Point &p : expands) {
                if(p.d == 0) {
                    if(p.i == 0 || p.i == h - 1 || p.j == 0 || p.j == w - 1) {
                        found = true;
                        break;
                    }
                    play(grid, new_expands, p.i - 1, p.j, d);
                    play(grid, new_expands, p.i + 1, p.j, d);
                    play(grid, new_expands, p.i, p.j - 1, d);
                    play(grid, new_expands, p.i, p.j + 1, d);
                } else {
                    new_expands.push_back({p.i, p.j, p.d - 1});
                }
            }
            expands = new_expands;
        }
        cout << answer << endl;
    }
    return 0;
}
