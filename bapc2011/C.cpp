#include <iostream>
#include <vector>

using namespace std;

struct Point {
    int64_t x, y;
};

int64_t orientation(const Point &a, const Point &b, const Point &c) {
    return (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);
}

int main()
{
    int T;
    cin >> T;
    while(T--) {
        int n;
        cin >> n;
        vector<Point> towers(n);
        for(int i = 0; i < n; i++) {
            cin >> towers[i].x >> towers[i].y;
        }
        int k;
        cin >> k;
        while(k--) {
            vector<bool> candidates(n, true);
            int m;
            cin >> m;
            for(int i = 0; i < m; i++) {
                int l, r;
                cin >> l >> r;
                candidates[l - 1] = false, candidates[r - 1] = false;
                for(int j = 0; j < n; j++) {
                    if(candidates[j]) {
                        candidates[j] = orientation(towers[j], towers[l - 1], towers[r - 1]) < 0;
                    }
                }
            }
            for(int i = 0; i < n; i++) {
                if(candidates[i]) {
                    cout << i + 1 << endl;
                }
            }
            cout << 0 << endl;
        }
    }
    return 0;
}
