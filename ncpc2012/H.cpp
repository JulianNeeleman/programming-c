#include <iostream>
#include <vector>
#include <stdint.h>

using namespace std;

int64_t MAX = 999999999;

int main()
{
    int64_t N, H, L;
    cin >> N >> H >> L;
    vector<int64_t> HI(N, MAX);
    vector<bool> column(N, false);
    vector<vector<bool> > adj(N, column);
    for(int64_t i = 0; i < H; i++) {
        int64_t x;
        cin >> x;
        HI[x] = 0;
    }
    for(int64_t i = 0; i < L; i++) {
        int64_t a, b;
        cin >> a >> b;
        adj[a][b] = true;
        adj[b][a] = true;
    }
    bool change = true;
    for(int64_t Q = 0; change; Q++) {
        change = false;
        for(int64_t i = 0; i < N; i++) {
            if(HI[i] == Q) {
                for(int64_t j = 0; j < N; j++) {
                    if(adj[i][j] && HI[j] == MAX) {
                        HI[j] = Q + 1;
                        change = true;
                    }
                }
            }
        }
    }
    int64_t best_HI = 0, index = 0;
    for(int64_t i = 0; i < N; i++) {
        if(HI[i] > best_HI) {
            best_HI = HI[i];
            index = i;
        }
    }
    cout << index << endl;
    return 0;
}
