#include <iostream>
#include <vector>
#include <stdint.h>
#include <math.h>
#include <algorithm>

using namespace std;

int main()
{
    int64_t X;
    cin >> X;
    vector<bool> line;
    string s;
    cin >> s;
    for(int64_t i = 0; i < s.size(); i++) {
        line.push_back(s[i] == 'M');
    }
    int64_t men = 0, women = 0, first = 0, second = 1;
    for(int64_t i = 0; i < s.size(); i++) {
        bool let_in = false;
        if((men > women && line[first]) ||
            (men < women && !line[first])) {
            let_in = line[second];
            second++;
        } else {
            let_in = line[first];
            second++;
            first = second - 1;
        }
        if(let_in) {
            men++;
        } else {
            women++;
        }
        if(abs(men - women) > X) {
            men--;
            break;
        }
    }
    cout << men + women << endl;
    return 0;
}
