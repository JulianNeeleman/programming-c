#include <iostream>
#include <stdint.h>
#include <vector>
#include <math.h>
#include <algorithm>

using namespace std;

struct Point {
    int x, y;

    Point operator-(const Point &rhs) const {
        return {x - rhs.x, y - rhs.y};
    }
};

int cross(const Point &p, const Point &q) {
    return p.x * q.y - p.y * q.x;
}

int orientation(const Point &p, const Point &q, const Point &r) {
    int result = (q.x - p.x) * (r.y - p.y) - (q.y - p.y) * (r.x - p.x);
    return (result > 0) - (result < 0);
}

bool parallel(const Point &p, const Point &q, const Point &r, const Point &s) {
    return cross(p - q, r - s) == 0;
}

bool identical(const Point &p, const Point &q, const Point &r, const Point &s) {
    return orientation(p, q, r) == 0 &&
        orientation(p, q, s) == 0 &&
        orientation(r, s, p) == 0 &&
        orientation(r, s, q) == 0;
}

// this only works for positive integers!
int ceil_div(int a, int b) {
    return (a + b - 1) / b;
}

int main() {
    int W, N;
    cin >> W >> N;
    vector<pair<Point, Point> > lines(N);
    int c = 0;
    bool all_parallel = true;
    for(int i = 0; i < N; i++) {
        cin >> lines[i].first.x >> lines[i].first.y >>
            lines[i].second.x >> lines[i].second.y;
        c++;
        for(int j = 0; j < i; j++) {
            if(identical(lines[i].first, lines[i].second, lines[j].first, lines[j].second)) {
                c--;
                break;
            } else if(all_parallel && !parallel(lines[i].first, lines[i].second, lines[j].first, lines[j].second)) {
                all_parallel = false;
            }
        }
    }
    if(all_parallel && W > c + 1) {
        cout << max(1, ceil_div(W, 2) - c) << endl;
    } else {
        cout << max(0, ceil_div(W, 2) - c) << endl;
    }
    return 0;
}
