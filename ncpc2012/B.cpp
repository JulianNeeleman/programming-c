#include <iostream>
#include <vector>
#include <stdint.h>

using namespace std;

int main()
{
    int64_t n;
    cin >> n;
    vector<int64_t> unsorted(n), sorted(n);
    for(int64_t i = 0; i < n; i++) {
        int64_t bread;
        cin >> bread;
        bread--;
        unsorted[bread] = i;
    }
    for(int64_t i = 0; i < n; i++) {
        int64_t bread;
        cin >> bread;
        bread--;
        sorted[bread] = i;
    }
    vector<bool> seen(n, false);
    int64_t cycle_count = 0;
    for(int64_t i = 0; i < n; i++) {
        if(!seen[i]) {
            seen[i] = true;
            int64_t cycle_length = 1, current = i;
            while(!seen[sorted[unsorted[current]]]) {
                cycle_length++;
                current = sorted[unsorted[current]];
                seen[current] = true;
            }
            cycle_count += cycle_length - 1;
        }
    }

    if(cycle_count % 2 == 0) {
        cout << "Possible" << endl;
    } else {
        cout << "Impossible" << endl;
    }
    return 0;
}
