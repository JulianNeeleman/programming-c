#include <iostream>
#include <stdint.h>
#include <vector>
#include <algorithm>

using namespace std;

struct House {
    int64_t p, r, c, i;

    bool operator<(const House& rhs) const {
        return r < rhs.r;
    }
};

int main()
{
    int64_t n;
    cin >> n;
    vector<House> houses(1, {-1, -1, -1, -1});
    for(int64_t i = 1; i <= n; i++) {
        House h;
        cin >> h.p >> h.r >> h.c;
        h.i = i;
        houses.push_back(h);
    }
    int64_t answer = 0;
    vector<House> sorted(houses.begin(), houses.end());
    sort(sorted.begin(), sorted.end());

    for(int64_t i = 1; i <= n; i++) {
        int64_t flow = sorted[i].r, j = sorted[i].i;
        vector<int64_t> houses_passed;
        while(flow <= houses[j].c && j != 0) {
            houses_passed.push_back(j);
            houses[j].c -= flow;
            j = houses[j].p;
        }
        if(j == 0) {
            answer++;
        } else {
            for(int64_t k = 0; k < houses_passed.size(); k++) {
                houses[houses_passed[k]].c += flow;
            }
        }
    }

    cout << answer << endl;

    return 0;
}
