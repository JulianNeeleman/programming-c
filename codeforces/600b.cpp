#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>

using namespace std;

int bs(const vector<int> &a, const int &b, const int &from, const int &to) {
    int middle = (from + to) / 2;
    if(a[middle - 1] <= b && a[middle] > b) {
        return middle;
    }
    if(a[middle] <= b) {
        return bs(a, b, middle, to);
    }
    return bs(a, b, from, middle);
}

int main()
{
    ios::sync_with_stdio(false);
    int n, m;
    cin >> n >> m;
    vector<int> a(n);
    for(int i = 0; i < n; i++) {
        cin >> a[i];
    }
    sort(a.begin(), a.end());
    for(int i = 0; i < m; i++) {
        int b;
        cin >> b;
        if(b < a[0]) {
            cout << 0 << endl;
        } else if(b >= a[n - 1]) {
            cout << n << endl;
        } else {
            cout << bs(a, b, 0, n) << " ";
        }
    }
    cout << endl;
    return 0;
}
