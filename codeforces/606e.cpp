#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <iomanip>

using namespace std;

struct Point {
    int64_t x, y;

    bool operator<(const Point &rhs) const {
        return x < rhs.x || (x == rhs.x && y > rhs.y);
    }

    bool operator==(const Point &rhs) const {
        return x == rhs.x && y == rhs.y;
    }
};

int64_t orientation(const Point &a, const Point &b, const Point &c) {
    int64_t result = (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);
    return (result > (int64_t)0) - (result < (int64_t)0);
}

bool intersect(const Point &a, const Point &b, const Point &c, const Point &d) {
    return orientation(c, d, a) != orientation(c, d, b);
}

double intersect_ratio(const Point &a, const Point &b, const Point &c, const Point &d) {
    int64_t x1 = a.x, x2 = b.x, x3 = c.x, x4 = d.x,
        y1 = a.y, y2 = b.y, y3 = c.y, y4 = d.y,
        num = ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)),
        denom = (x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4);
    return (long double)x4 * (long double)num / (long double)denom;
}

vector<Point> convex_hull(vector<Point> &points, const int &n) {
    sort(points.begin(), points.end());
    vector<Point> hull(2 * n);
    int k = 0;
    for(int i = 0; i < n; i++) {
        while(k >= 2 && orientation(hull[k - 2], hull[k - 1], points[i]) <= 0) k--;
        hull[k++] = points[i];
    }
    for(int i = n - 2, t = k + 1; i >= 0; i--) {
        while(k >= t && orientation(hull[k - 2], hull[k - 1], points[i]) <= 0) k--;
        hull[k++] = points[i];
    }
    hull.resize(k - 1);
    return hull;
}

int main()
{
    ios::sync_with_stdio(false);
    int n, p, q;
    cin >> n >> p >> q;
    vector<Point> jobs(n);
    int64_t max_x = 0, max_y = 0;
    for(int i = 0; i < n; i++) {
        cin >> jobs[i].x >> jobs[i].y;
        max_x = max(max_x, jobs[i].x);
        max_y = max(max_y, jobs[i].y);
    }
    Point origin = {0, 0};
    jobs.push_back(origin);
    jobs.push_back({max_x, 0});
    jobs.push_back({0, max_y});
    vector<Point> hull = convex_hull(jobs, n + 3);
    sort(hull.begin(), hull.end());
    hull.erase(remove(hull.begin(), hull.end(), origin), hull.end());
    n = hull.size();
    for(int i = 0; i < n - 1; i++) {
        if(intersect(hull[i], hull[i + 1], origin, {p, q})) {
            cout << setprecision(20) << intersect_ratio(hull[i], hull[i + 1], origin, {p, q}) << endl;
            return 0;
        }
    }

    return 0;
}
